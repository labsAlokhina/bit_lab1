<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/7/2018
 * Time: 12:11 AM
 */

class BabbageAttack
{
    private const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    private const MOST_FREQUENT_LETTER = 'E';

    public function attack(string $text): string
    {
        $keyLength = $this->getKeyLengthMatchIndexMethod($text);
        $keyLetters = $this->getMostFrequentKeyLetters($text, $keyLength);
        $keyDistances = $this->getKeyLettersDistances($keyLength, $keyLetters);
        $hackedText = $this->hackText($text, $keyLength, $keyDistances);
        return $hackedText;
    }

    private function getKeyLengthMatchIndexMethod(string $text): int
    {
        $maxKeyLength = 15;
        $group = [];
        for ($i = 2; $i < $maxKeyLength; $i++)
        {
            $group[$i] = [
                'length' => 0,
                'letters' => [],
                'matchIndex' => 0
            ];
        }
        $textLength = strlen($text);
        $maxMatchIndex = 0;
        $bestKeyLength = 0;
        foreach ($group as $keyLength => &$info)
        {
            $groupCounter = $keyLength;
            for($i = 0; $i < $textLength; $i ++)
            {
                $currentLetter = $text[$i];
                while (!$this->isInAlphabet($currentLetter) && ($i < $textLength - 1)) {
                        $i++;
                        $currentLetter = $text[$i];
                }
                if ($this->isInAlphabet($currentLetter)) {
                    if ($groupCounter === $keyLength) {
                        $info['length']++;
                        $this->addLetterRepeat($currentLetter, $info['letters']);
                        $groupCounter = 1;
                    } else {
                        $groupCounter++;
                    }
                }
            }
            foreach ($info['letters'] as $letter => $repeats) {
                $info['matchIndex'] += $repeats * ($repeats - 1);
            }
            $info['matchIndex'] = $info['matchIndex']/($info['length']*($info['length'] - 1));
            echo $keyLength . " = " . $info['matchIndex'] . "\n";
            if ($info['matchIndex'] > $maxMatchIndex) {
                $maxMatchIndex = $info['matchIndex'];
                $bestKeyLength = $keyLength;
            }
        }
        echo 'Max index ' . $maxMatchIndex . '. Key length ' . $bestKeyLength . "\n";
        return $bestKeyLength;
    }

    private function getMostFrequentKeyLetters(string $text, int $keyLength): array
    {
        $textLength = strlen($text);
        $keyLetters = [];
        for($i = 0; $i < $keyLength; $i++)
        {
            $lettersRepeats = [];
            $groupCounter = $keyLength;
            for ($j = $i; $j < $textLength; $j++)
            {
                $currentLetter = $text[$j];
                while (!$this->isInAlphabet($currentLetter) && ($j < $textLength - 1)) {
                    $j++;
                    $currentLetter = $text[$j];
                }
                if ($this->isInAlphabet($currentLetter)) {
                    if ($groupCounter === $keyLength) {
                        $this->addLetterRepeat($currentLetter, $lettersRepeats);
                        $groupCounter = 1;
                    } else {
                        $groupCounter++;
                    }
                }
            }
            $keyLetters[$i] = array_search(max($lettersRepeats),$lettersRepeats);
        }
        return $keyLetters;
    }

    private function getKeyLettersDistances(int $keyLength, array $keyLetters): array
    {
        $keyDistances = [];
        $mostFrequentLetterNumber = $this->getLetterNumberInAlphabet(self::MOST_FREQUENT_LETTER);
        for ($i = 0; $i < $keyLength; $i++)
        {
            $letterNumber = $this->getLetterNumberInAlphabet($keyLetters[$i]);
            $keyDistances[$i] = $letterNumber - $mostFrequentLetterNumber;
        }
        return $keyDistances;
    }

    private function hackText(string $text, int $keyLength, array $keyDistances): string
    {
        $hackedText = '';
        $textLength = strlen($text);
        $groupCounter = 0;
        for($i = 0; $i < $textLength; $i++)
        {
            $currentLetter = $text[$i];
            $currentLetterNumber = $this->getLetterNumberInAlphabet($currentLetter);
            if ($this->isInAlphabet($currentLetter)) {
                $hackedLetterNumber = $currentLetterNumber - $keyDistances[$groupCounter];
                $hackedText .= self::ALPHABET[$hackedLetterNumber];
                if ($groupCounter < $keyLength - 1) {
                    $groupCounter++;
                } else {
                    $groupCounter = 0;
                }
            } else {
                $hackedText[$i] = $text[$i];
            }
        }
        return $hackedText;
    }

    private function isInAlphabet(string $letter): bool
    {
        return !(strpos(self::ALPHABET, $letter) === false);
    }

    private function getLetterNumberInAlphabet(string $letter): int
    {
        return strpos(self::ALPHABET, $letter);
    }

    private function addLetterRepeat(string $letter, array &$lettersRepeatsArray): void
    {
        if (array_key_exists($letter, $lettersRepeatsArray)) {
            $lettersRepeatsArray[$letter]++;
        } else {
            $lettersRepeatsArray[$letter] = 1;
        }
    }
}