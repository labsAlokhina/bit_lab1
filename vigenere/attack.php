<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/7/2018
 * Time: 12:10 AM
 */

declare(strict_types=1);
include __DIR__ . '\BabbageAttack.php';

$babbageAttack = new BabbageAttack();
$textToDecrypt = file_get_contents(__DIR__ . '\text_encrypted.txt');
$hackedText = $babbageAttack->attack($textToDecrypt);
echo "\n" . $hackedText;