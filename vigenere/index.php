<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/5/2018
 * Time: 9:05 PM
 */
declare(strict_types=1);
include __DIR__ . '\VigenereEncoder.php';

$keyWord = 'ALOKHINA';
$encoder = new VigenereEncoder($keyWord);

$textToEncrypt = file_get_contents(__DIR__ . '\text_to_encrypt.txt');
$encodedText = $encoder->encode(strtoupper($textToEncrypt));
file_put_contents(__DIR__ . '\text_encrypted.txt', $encodedText);
echo "Encoded successfully!\n";

$textToDecrypt = file_get_contents(__DIR__ . '\text_encrypted.txt');
$decodedText = $encoder->decode($textToDecrypt);
file_put_contents(__DIR__ . '\text_decrypted.txt', $decodedText);
echo "Decoded successfully!";




