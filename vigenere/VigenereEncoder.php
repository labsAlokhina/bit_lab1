<<?php
/**
 * Created by PhpStorm.
 * User: anast
 * Date: 10/5/2018
 * Time: 9:22 PM
 */

class VigenereEncoder
{
    private $keyWord;
    private const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public function __construct(string $keyWord)
    {
        $this->keyWord = $keyWord;
    }

    public function encode(string $text): string
    {
        $table = $this->getTable();
        $keyWordSymbolNumber = 0;
        $encodedText = '';
        for($i = 0; $i < strlen($text); $i++)
        {
            $keyWordSymbol = $this->keyWord[$keyWordSymbolNumber];
            if (array_key_exists($text[$i], $table)){
                $encodedText .= $table[$text[$i]][$keyWordSymbol];
                $keyWordSymbolNumber = ($keyWordSymbolNumber < strlen($this->keyWord) - 1) ? $keyWordSymbolNumber + 1 : 0;
            } else {
                $encodedText .= $text[$i];
            }
        }
        return $encodedText;
    }

    public function decode(string $text): string
    {
        $table = $this->getTable();
        $keyWordSymbolNumber = 0;
        $encodedText = '';
        for($i = 0; $i < strlen($text); $i++)
        {
            $keyWordSymbol = $this->keyWord[$keyWordSymbolNumber];
            if (array_key_exists($text[$i], $table)){
                $encodedText .= array_keys($table[$keyWordSymbol], $text[$i])[0];
                $keyWordSymbolNumber = ($keyWordSymbolNumber < strlen($this->keyWord) - 1) ? $keyWordSymbolNumber + 1 : 0;
            } else {
                $encodedText .= $text[$i];
            }
        }
        return $encodedText;
    }

    private function getTable(): array
    {
        $alphabet = self::ALPHABET;
        $table = [];
        for($i = 0; $i < strlen($alphabet); $i++)
        {
            $vigenereShift = $i;
            $table[$alphabet[$i]] = [];
            for ($j = 0; $j < strlen($alphabet); $j++)
            {
                $shift = $j + $vigenereShift;
                $table[$alphabet[$i]][$alphabet[$j]] = (($shift) < strlen($alphabet)) ? $alphabet[$shift] : $alphabet[$shift - strlen($alphabet)];
            }
        }
        return $table;
    }
}